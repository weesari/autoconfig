# Autoconfig your shell

## Features
1. Install tmux
2. Install ohmytmux
3. Enable mode-keys vi
4. Install zsh
5. Install ohmyzsh
6. Install fzf
7. Resize ctrl-r fzf window to 100%
