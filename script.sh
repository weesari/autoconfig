#!/bin/bash

# Actualizar repositorios
sudo apt-get update

# Instalar tmux
if ! command -v tmux >/dev/null; then
    sudo apt-get install -y tmux
else
    echo "tmux ya está instalado."
fi

# Instalar zsh
if ! command -v zsh >/dev/null; then
    sudo apt-get install -y zsh
else
    echo "zsh ya está instalado."
fi

# Instalar ohmytmux
if [ ! -d "$HOME/.tmux" ]; then
    git clone https://github.com/gpakosz/.tmux.git "$HOME/.tmux"
    ln -s -f "$HOME/.tmux/.tmux.conf" "$HOME/.tmux.conf"
    cp "$HOME/.tmux/.tmux.conf.local" "$HOME/"
else
    echo "ohmytmux ya está instalado."
fi

# Instalar fzf
if ! command -v fzf >/dev/null; then
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install --all
else
    echo "fzf ya está instalado."
fi

# Descomentar la línea del mode-keys vi en .tmux.conf.local
sed -i 's/#set -g mode-keys vi/set -g mode-keys vi/' "$HOME/.tmux.conf.local"

# Instalar ohmyzsh
if [ ! -d "$HOME/.oh-my-zsh" ]; then
    curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh -o install_ohmyzsh.sh
    sed -i 's/exec zsh -l//' "install_ohmyzsh.sh"
    sh "install_ohmyzsh.sh"
else
    echo "ohmyzsh ya está instalado."
fi

# Configurar fzf para mostrar al 100% la lista de búsqueda reversiva al presionar ctrl-r
echo 'export FZF_CTRL_R_OPTS="--height 100%"' >> "$HOME/.zshrc"

# Cambiar el tema del zsh usando el sistema de tema de ohmyzsh
sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="josh"/' "$HOME/.zshrc"

# Aplicar los cambios
exec zsh -l

echo "Instalación y configuración completadas. Reinicia la terminal para aplicar los cambios."
